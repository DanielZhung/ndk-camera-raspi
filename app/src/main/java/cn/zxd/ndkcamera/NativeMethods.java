package cn.zxd.ndkcamera;

import android.view.Surface;

public class NativeMethods {

    static {
        System.loadLibrary("native-lib");
    }

    public static native void preview(Surface surface, int width, int height);

}
