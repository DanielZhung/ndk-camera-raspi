#include <jni.h>
#include <android/log.h>
#include <android/native_window_jni.h>
#include <camera/NdkCameraManager.h>
#include <media/NdkImageReader.h>

#define  LOG_TAG    "Raspiberry PI3B"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

#ifndef MIN
#  define MIN(a,b)  ((a) > (b) ? (b) : (a))
#endif

#ifndef MAX
#  define MAX(a,b)  ((a) < (b) ? (b) : (a))
#endif

//

static const int kMaxChannelValue = 262143;

static ACameraDevice *cameraDevice = NULL;
static ACameraDevice_StateCallbacks deviceStateCallbacks;

static ANativeWindow *previewSurfaceWindow = NULL;
static AImageReader *imageReader = NULL;
static AImageReader_ImageListener imageReaderImageListener;

static ACameraCaptureSession_stateCallbacks captureSessionStateCallbacks;

//
static void camera_device_on_disconnected(void *context, ACameraDevice *device) {
    LOGI("Camera(id: %s) is diconnected.\n", ACameraDevice_getId(device));
    if (cameraDevice != NULL) {
        ACameraDevice_close(cameraDevice);
    }
}

static void camera_device_on_error(void *context, ACameraDevice *device, int error) {
    LOGE("Error(code: %d) on Camera(id: %s).\n", error, ACameraDevice_getId(device));
    if (cameraDevice != NULL) {
        ACameraDevice_close(cameraDevice);
    }
}

static void camera_capture_session_on_active(void *context, ACameraCaptureSession *session) {
    LOGE("camera_capture_session_on_active");
}

static void camera_capture_session_on_ready(void *context, ACameraCaptureSession *session) {
    LOGE("camera_capture_session_on_ready");
}

static void camera_capture_session_on_closed(void *context, ACameraCaptureSession *session) {
    LOGE("camera_capture_session_on_closed");
}

static inline uint32_t YUV2RGB(int nY, int nU, int nV) {
    nY -= 16;
    nU -= 128;
    nV -= 128;
    if (nY < 0) nY = 0;

    // This is the floating point equivalent. We do the conversion in integer
    // because some Android devices do not have floating point in hardware.
    // nR = (int)(1.164 * nY + 1.596 * nV);
    // nG = (int)(1.164 * nY - 0.813 * nV - 0.391 * nU);
    // nB = (int)(1.164 * nY + 2.018 * nU);

    int nR = (int) (1192 * nY + 1634 * nV);
    int nG = (int) (1192 * nY - 833 * nV - 400 * nU);
    int nB = (int) (1192 * nY + 2066 * nU);

    nR = MIN(kMaxChannelValue, MAX(0, nR));
    nG = MIN(kMaxChannelValue, MAX(0, nG));
    nB = MIN(kMaxChannelValue, MAX(0, nB));

    nR = (nR >> 10) & 0xff;
    nG = (nG >> 10) & 0xff;
    nB = (nB >> 10) & 0xff;

    return 0xff000000 | (nR << 16) | (nG << 8) | nB;
}

static void show_preview_image(ANativeWindow_Buffer *buf, AImage *image) {
    AImageCropRect srcRect;
    AImage_getCropRect(image, &srcRect);
    int32_t yStride, uvStride;
    uint8_t *yPixel, *uPixel, *vPixel;
    int32_t yLen, uLen, vLen;
    AImage_getPlaneRowStride(image, 0, &yStride);
    AImage_getPlaneRowStride(image, 1, &uvStride);
    AImage_getPlaneData(image, 0, &yPixel, &yLen);
    AImage_getPlaneData(image, 1, &vPixel, &vLen);
    AImage_getPlaneData(image, 2, &uPixel, &uLen);
    int32_t uvPixelStride;
    AImage_getPlanePixelStride(image, 1, &uvPixelStride);

    int32_t height = MIN(buf->height, (srcRect.bottom - srcRect.top));
    int32_t width = MIN(buf->width, (srcRect.right - srcRect.left));

    uint32_t *out = static_cast<uint32_t *>(buf->bits);
    for (int32_t y = 0; y < height; y++) {
        const uint8_t *pY = yPixel + yStride * (y + srcRect.top) + srcRect.left;

        int32_t uv_row_start = uvStride * ((y + srcRect.top) >> 1);
        const uint8_t *pU = uPixel + uv_row_start + (srcRect.left >> 1);
        const uint8_t *pV = vPixel + uv_row_start + (srcRect.left >> 1);

        for (int32_t x = 0; x < width; x++) {
            const int32_t uv_offset = (x >> 1) * uvPixelStride;
            out[x] = YUV2RGB(pY[x], pU[uv_offset], pV[uv_offset]);
        }
        out += buf->stride;
    }
}

static void on_image_available(void* context, AImageReader* reader){
    AImage *image;
    AImageReader_acquireLatestImage(reader, &image);
    ANativeWindow_Buffer surfaceBuffer;
    int mediaStatus = ANativeWindow_lock(previewSurfaceWindow, &surfaceBuffer, NULL);
    if (mediaStatus != 0) {
        LOGE("Fail to lock surface (error:%d).\n", mediaStatus);
        return;
    }
    show_preview_image(&surfaceBuffer, image);
    ANativeWindow_unlockAndPost(previewSurfaceWindow);
    AImage_delete(image);
}

extern "C" JNIEXPORT void
JNICALL
Java_cn_zxd_ndkcamera_NativeMethods_preview(
        JNIEnv *env,
        jobject thiz,
        jobject previewSurface,
        int width,
        int height) {
    ACameraManager *cameraManager = ACameraManager_create();
    ACameraIdList *cameraIdList = new ACameraIdList();
    camera_status_t cameraStatus = ACameraManager_getCameraIdList(cameraManager, &cameraIdList);
    if (cameraStatus != ACAMERA_OK) {
        LOGE("ACameraManager_getCameraIdList Error, status:%d.", cameraStatus);
    }
    if (cameraIdList->numCameras < 1) {
        LOGE("Could NOT found Camera Device.");
    }
    deviceStateCallbacks.onDisconnected = camera_device_on_disconnected;
    deviceStateCallbacks.onError = camera_device_on_error;
    cameraStatus = ACameraManager_openCamera(cameraManager, cameraIdList->cameraIds[0],
                                             &deviceStateCallbacks, &cameraDevice);
    if (cameraStatus != ACAMERA_OK) {
        LOGE("Failed to open camera device (id: %s).\n", cameraIdList->cameraIds[0]);
    }

    previewSurfaceWindow = ANativeWindow_fromSurface(env, previewSurface);
    if (previewSurfaceWindow == NULL) {
        LOGE("ANativeWindow_fromSurface error");
        return;
    }

    ANativeWindow *nativeWindow = NULL;

    media_status_t mediaStatus = AImageReader_new(width, height, AIMAGE_FORMAT_YUV_420_888, 10,
                                                  &imageReader);
    if (mediaStatus != AMEDIA_OK) {
        LOGE("Failed to create image reader (error: %d).\n", mediaStatus);
        return;
    }

    imageReaderImageListener.onImageAvailable = on_image_available;
    mediaStatus = AImageReader_setImageListener(imageReader, &imageReaderImageListener);
    if (mediaStatus != AMEDIA_OK) {
        LOGE("Failed to set image listener (error: %d).\n", mediaStatus);
        return;
    }

    mediaStatus = AImageReader_getWindow(imageReader, &nativeWindow);
    if (mediaStatus != AMEDIA_OK) {
        LOGE("Failed to get image reader window (error: %d).\n", mediaStatus);
        return;
    }
    ACameraOutputTarget *cameraOutputTarget = NULL;
    cameraStatus = ACameraOutputTarget_create(nativeWindow,
                                                              &cameraOutputTarget);
    if (cameraStatus != ACAMERA_OK) {
        LOGE("Failed to create camera output target (error: %d).\n", cameraStatus);
        return;
    }
    ACaptureRequest *captureRequest = NULL;
    cameraStatus = ACameraDevice_createCaptureRequest(cameraDevice, TEMPLATE_RECORD,
                                                      &captureRequest);
    if (cameraStatus != ACAMERA_OK) {
        LOGE("Failed to create capture request (error: %d).\n", cameraStatus);
        return;
    }
    cameraStatus = ACaptureRequest_addTarget(captureRequest, cameraOutputTarget);
    if (cameraStatus != ACAMERA_OK) {
        LOGE("Failed to add target (error: %d).\n", cameraStatus);
        return;
    }
    ACaptureSessionOutputContainer *captureSessionOutputContainer = NULL;
    cameraStatus = ACaptureSessionOutputContainer_create(&captureSessionOutputContainer);
    if (cameraStatus != ACAMERA_OK) {
        LOGE("Failed to create output container (error: %d).\n", cameraStatus);
        return;
    }
    ACaptureSessionOutput *sessionOutput = NULL;
    cameraStatus = ACaptureSessionOutput_create(nativeWindow, &sessionOutput);
    if (cameraStatus != ACAMERA_OK) {
        LOGE("Failed to create output session (error: %d).\n", cameraStatus);
        return;
    }
    cameraStatus = ACaptureSessionOutputContainer_add(captureSessionOutputContainer,
                                                      sessionOutput);
    if (cameraStatus != ACAMERA_OK) {
        LOGE("Failed to session container add output (error: %d).\n", cameraStatus);
        return;
    }
    captureSessionStateCallbacks.onActive = camera_capture_session_on_active;
    captureSessionStateCallbacks.onReady = camera_capture_session_on_ready;
    captureSessionStateCallbacks.onClosed = camera_capture_session_on_closed;
    ACameraCaptureSession *captureSession = NULL;
    cameraStatus = ACameraDevice_createCaptureSession(cameraDevice,
                                                      captureSessionOutputContainer,
                                                      &captureSessionStateCallbacks,
                                                      &captureSession);
    if (cameraStatus != ACAMERA_OK) {
        LOGE("Failed to create camera capture session (reason: %d).\n", cameraStatus);
        return;
    }

    cameraStatus = ACameraCaptureSession_setRepeatingRequest(captureSession, NULL, 1,&captureRequest, NULL);
    if (cameraStatus != ACAMERA_OK) {
        LOGE("Failed to repeat request (reason: %d).\n", cameraStatus);
        return;
    }
}
