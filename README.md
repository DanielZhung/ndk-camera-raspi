# Android Things 树莓派3B 摄像头预览 #

## 说明 ##
  在树莓派3B上运行Android Things，使用Camera API2 连接摄像头，ImageReader 获取到摄像头YUV数据后，转换成RGB数据展示到SurfaceView上。
  可以使用图像处理库，例如OpenCV处理转换的RGB数据
  
### 预览 ###
  ![screenshot](demo.gif)
  
## [WTFPL](https://en.wikipedia.org/wiki/WTFPL) ##